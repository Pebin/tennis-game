package tennis;

import tennis.commands.*;

import java.util.HashMap;
import java.util.Map;

public class Main {
    public static final String PLAYER1NAME = "A";
    public static final String PLAYER2NAME = "B";

    public static void main(String[] args) {

        InputParser ip = new InputParser(System.in, prepareCommands());
        Game game = Game.getInstance();

        System.out.printf("%s and %s exchanged greetings. The match may begin.%s", PLAYER1NAME, PLAYER2NAME,
                System.getProperty("line.separator"));

        while (game.isRunning()) {
            System.out.print("> ");
            Command command = ip.readAndMapCommand();
            String output;

            if (command == null) {
                output = getUsage();
            } else {
                output = command.execute();
            }

            System.out.println(output);
        }
    }

    private static Map<String, Command> prepareCommands() {
        Map<String, Command> commandMap = new HashMap<>();

        commandMap.put("EXIT", new ExitCommand());
        commandMap.put("NEW GAME", new NewGameCommand());
        commandMap.put("SHOW SCORE", new ShowScoreCommand());
        commandMap.put("SHOW WINNER", new ShowWinnerCommand());
        commandMap.put(String.format("PLAYER %s SCORED", PLAYER1NAME), new ScoreCommand(PLAYER1NAME));
        commandMap.put(String.format("PLAYER %s SCORED", PLAYER2NAME), new ScoreCommand(PLAYER2NAME));

        return commandMap;
    }

    private static String getUsage() {
        StringBuilder usage = new StringBuilder();

        usage.append("Available commands:").append(System.getProperty("line.separator"))
                .append("------------------------------------------------").append(System.getProperty("line.separator"))
                .append("new game").append(System.getProperty("line.separator"))
                .append("exit").append(System.getProperty("line.separator"))
                .append("player ").append(PLAYER1NAME).append(" scored").append(System.getProperty("line.separator"))
                .append("player ").append(PLAYER2NAME).append(" scored").append(System.getProperty("line.separator"))
                .append("show score").append(System.getProperty("line.separator"))
                .append("show winner").append(System.getProperty("line.separator"));

        return usage.toString();
    }
}
