package tennis;

import tennis.exceptions.IllegalPointsStateException;

/**
 * Created by petr on 24. 6. 2015.
 */
public class Player {
    private String name;
    private Points points;

    public Player(String name) {
        this.name = name;
        this.points = new Points();
    }

    public String getName() {
        return name;
    }

    public Points getPoints() {
        return points;
    }

    public void resetPoints() {
        this.points = new Points();
    }
}
