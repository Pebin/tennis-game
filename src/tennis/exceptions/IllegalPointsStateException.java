package tennis.exceptions;

/**
 * Created by barton on 26.6.2015.
 */
public class IllegalPointsStateException extends Exception {
    public IllegalPointsStateException() {
    }

    public IllegalPointsStateException(String message) {
        super(message);
    }

    public IllegalPointsStateException(String message, Throwable cause) {
        super(message, cause);
    }

    public IllegalPointsStateException(Throwable cause) {
        super(cause);
    }
}
