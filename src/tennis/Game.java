package tennis;

/**
 * Created by petr on 24. 6. 2015.
 */
public class Game {
    private static Game instance;

    private Player p1;
    private Player p2;

    private boolean running;

    private Game() {
        this.p1 = new Player(Main.PLAYER1NAME);
        this.p2 = new Player(Main.PLAYER2NAME);
        this.running = true;
    }

    public static Game getInstance(){
        if (instance == null) {
            instance = new Game();
        }

        return instance;
    }

    public void restart() {
        p1.resetPoints();
        p2.resetPoints();
    }

    public void stop() {
        running = false;
    }

    public boolean isRunning() {
        return running;
    }

    public Player getP2() {
        return p2;
    }

    public Player getP1() {
        return p1;
    }
}
