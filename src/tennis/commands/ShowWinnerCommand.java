package tennis.commands;

import tennis.GameScoreManager;
import tennis.Player;

/**
 * Created by petr on 24. 6. 2015.
 */
public class ShowWinnerCommand implements Command {

    @Override
    public String execute() {
        Player winner = GameScoreManager.getWinner();
        String message;

        if (winner == null) {
            message = "No one";
        } else {
            message = "The winner of this tennis match is " + winner.getName();
        }

        return message;
    }
}
