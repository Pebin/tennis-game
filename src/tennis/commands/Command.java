package tennis.commands;

import tennis.Game;

/**
 * Created by petr on 24. 6. 2015.
 */
public interface Command {
    String execute();
}