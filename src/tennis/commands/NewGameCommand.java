package tennis.commands;

import tennis.Game;

/**
 * Created by petr on 24. 6. 2015.
 */
public class NewGameCommand implements Command {

    @Override
    public String execute() {
        Game.getInstance().restart();

        return "This match is going to be something to remember.";
    }
}
