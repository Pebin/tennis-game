package tennis.commands;

import tennis.Game;

/**
 * Created by petr on 24. 6. 2015.
 */
public class ExitCommand implements Command {
    @Override
    public String execute() {
        Game.getInstance().stop();

        return "We shall meet again.";
    }
}
