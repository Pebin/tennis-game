package tennis.commands;

import tennis.GameScoreManager;
import tennis.exceptions.IllegalPointsStateException;

import java.util.Random;

/**
 * Created by petr on 24. 6. 2015.
 */
public class ScoreCommand implements Command {

    private static final String messages[] = {"Eso!", "That's what I call backhand!", "Unbelievable!", "Nice shot!",
            "Wow. I couldn't even see the ball.", "This deserves celebration!",
            "I have never seen anything like this before!"};

    private String playerName;

    public ScoreCommand(String playerName) {
        this.playerName = playerName;
    }

    @Override
    public String execute() {
        boolean playerScored = false;
        try {
            playerScored = GameScoreManager.score(playerName);
        } catch (IllegalPointsStateException e) {
            return "ERROR: points are in wrong state. Please start new game.";
        }

        if (playerScored) {
            int messageNumber = new Random().nextInt(messages.length);
            return messages[messageNumber];
        } else {
            return "The match already ended";
        }
    }
}
