package tennis.commands;

import tennis.GameScoreManager;
import tennis.ScoreFormat;

/**
 * Created by petr on 24. 6. 2015.
 */
public class ShowScoreCommand implements Command {
    @Override
    public String execute() {
        String output;
        if (GameScoreManager.getWinner() == null) {
            output = "Score: " + ScoreFormat.get();
        } else {
            output = new ShowWinnerCommand().execute();
        }

        return output;
    }
}
