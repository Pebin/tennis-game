package tennis;

/**
 * Created by petr on 25. 6. 2015.
 */
public class ScoreFormat {
    public static String get() {
        Game game = Game.getInstance();
        String output;

        Points p1Points = game.getP1().getPoints();
        Points p2Points = game.getP2().getPoints();

        if (p1Points.getPointNumber() == 3 && p2Points.getPointNumber() == 3) {
            output = "deuce";
        } else if (p1Points.getPointNumber() == p2Points.getPointNumber()) {
            output = p1Points + " all";
        } else {
            output = p1Points + " - " + p2Points;
        }

        return output;
    }
}
