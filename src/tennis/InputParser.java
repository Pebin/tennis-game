package tennis; /**
 * Created by petr on 24. 6. 2015.
 */

import tennis.commands.*;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class InputParser {
    private Scanner scanner;
    private Map<String, Command> commandMap;

    public InputParser(InputStream inputStream, Map<String, Command> commandMap) {
        this.scanner = new Scanner(inputStream);
        this.commandMap = commandMap;
    }

    public Command readAndMapCommand(){
        String inputLine = scanner.nextLine();
        return processLine(inputLine);
    }

    private Command processLine(String line){
        line = line.toUpperCase();

        Command command = null;

        if (commandMap.containsKey(line)){
            command = commandMap.get(line);
        }

        return command;
    }
}
