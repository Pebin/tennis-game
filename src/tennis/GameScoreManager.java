package tennis;

import tennis.exceptions.IllegalPointsStateException;

import java.util.Objects;

/**
 * Created by petr on 25. 6. 2015.
 */
public class GameScoreManager {
    public static boolean score(String playerName) throws IllegalPointsStateException {
        Game game = Game.getInstance();

        if (getWinner() == null) {
            Points p1Points = game.getP1().getPoints();
            Points p2Points = game.getP2().getPoints();

            if (p1Points.getPointNumber() == 4 && playerName.equals(game.getP2().getName())){
                p1Points.removePoint();
            } else if (p2Points.getPointNumber() == 4 && playerName.equals(game.getP1().getName())){
                p2Points.removePoint();
            } else {
                if (Objects.equals(playerName, game.getP1().getName())) {
                    p1Points.addPoint();
                } else if (Objects.equals(playerName, game.getP2().getName())) {
                    p2Points.addPoint();
                }
            }
            return true;
        }
        return false;
    }

    public static Player getWinner(){
        Game game = Game.getInstance();

        int p1Points = game.getP1().getPoints().getPointNumber();
        int p2Points = game.getP2().getPoints().getPointNumber();

        if (p1Points > 3 || p2Points > 3) {
            if (Math.abs(p1Points - p2Points) >= 2){
                if (p1Points > p2Points) {
                    return game.getP1();
                }
                return game.getP2();
            }
        }

        return null;
    }
}
