package tennis;

import tennis.exceptions.IllegalPointsStateException;

/**
 * Created by petr on 24. 6. 2015.
 */
public class Points {
    private static final String[] pointRepresentation = {"0", "15", "30", "40", "A", "-"};
    private int pointNumber;

    public Points() {
        pointNumber = 0;
    }

    public int getPointNumber() {
        return pointNumber;
    }

    public void addPoint() throws IllegalPointsStateException {
        pointNumber += 1;

        if (pointNumber > 5)
            throw new IllegalPointsStateException(
                    String.format("Internal point representation reached point %d, when 5 is maximum.", pointNumber));
    }

    public void removePoint() throws IllegalPointsStateException  {
        pointNumber -= 1;

        if (pointNumber < 0)
            throw new IllegalPointsStateException(
                    String.format("Internal point representation reached point %d, when 0 is minimum.", pointNumber));
    }

    @Override
    public String toString() {
        return pointRepresentation[getPointNumber()];
    }
}
